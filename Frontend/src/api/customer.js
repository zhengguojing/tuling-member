import { axios } from '@/libs/api.request'

// #region  客户
export const SearchCustomer = form => {
  return axios.post('api/Customer/SearchCustomer', form)
}

export const SaveCustomer = form => {
  return axios.post('api/Customer/SaveCustomer', form)
}

export const DeleteCustomer = id => {
  return axios.post('api/Customer/DeleteCustomer/' + id)
}

export const GetCustomer = id => {
  return axios.get('api/Customer/GetCustomer/' + id)
}
export const SearchCustomerPayLog = form => {
  return axios.post('api/Customer/SearchCustomerPayLog', form)
}
export const AddCustomerPayLog = form => {
  return axios.post('api/Customer/AddCustomerPayLog', form)
}

// #endregion
