﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TulingMember.Application
{
    public class WechatAccessTokenDto
    {
        public int? errcode { get; set; }
        public string errmsg { get; set; }
        public int? expires_in { get; set; }
        public string access_token { get; set; } 

    }

    public class GoodsItem
    {
        
    }

    public class Room_infoItem
    {
        /// <summary>
        /// 直播房间名
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int roomid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string cover_img { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string share_img { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int live_status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long start_time { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long end_time { get; set; }
        /// <summary>
        /// 里斯
        /// </summary>
        public string anchor_name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<GoodsItem> goods { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int live_type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int close_goods { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int close_comment { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int close_kf { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int close_replay { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int is_feeds_public { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string creater_openid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string feeds_img { get; set; }
    }

    public class LiveRoomDto
    {
        /// <summary>
        /// 
        /// </summary>
        public int? errcode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string errmsg { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? total { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<Room_infoItem> room_info { get; set; }
    }
}
