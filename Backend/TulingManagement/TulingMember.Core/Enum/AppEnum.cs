﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TulingMember.Core
{
    public class AppEnum
    {
        public static string GetGender(Gender value)
        {

            if (value == Gender.Male)
            {
                return "男";
            }
            else if (value == Gender.FeMale)
            {
                return "女";
            }
            else
            {
                return "";
            }
        }
        public static string GetGender(int value)
        {

            if (value == 0)
            {
                return "男";
            }
            else if (value == 1)
            {
                return "女";
            }
            else
            {
                return "";
            }
        }
    }
    public enum UserType
    {

        System = 9, 
        Merchant = 0
    }

    public enum Gender
    {
        
        Male = 1,
        FeMale = 0,
    }
    
    /// <summary>
    /// 商品变更
    /// </summary>
    public enum ProductChangeTag
    {
        None=0,
        In = 1,
        Out = 2,
        Check = 3,
    }
    /// <summary>
    /// 销售单据类型
    /// </summary>
    public enum OrderType
    {
        None=0,
        Out = 1,
        Back = 2, 
    }
}
